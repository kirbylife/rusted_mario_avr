# export RUST_TARGET_PATH=`pwd`
# export XARGO_RUST_SRC=/home/kirbylife/Aplicaciones/rust-avr/rust/src

# rustup run avr-toolchain xargo build --target avr-atmega328p --release
cargo build -Z build-std=core --target avr-atmega328p.json --release
avr-objcopy -O ihex -R .eeprom target/avr-atmega328p/release/rusted_mario_avr.elf output.hex
avrdude -p atmega328p -c arduino -P /dev/ttyACM0 -U flash:w:output.hex:i
