#![feature(llvm_asm, lang_items, core_intrinsics)]
#![no_std]
#![no_main]

/*
PORTC{0..5} Analog
PORTC6 Reset

PORTB{0..5} Digital {8..13}
PORTD{0..7} Digital {0..7}

PORTB5 LED_BUILTIN
*/

extern crate avr_std_stub;
extern crate ruduino;

// use core::ptr::{read, read_volatile, write_volatile};
use ruduino::cores::current as avr_core;
use ruduino::legacy::serial;
use ruduino::Register;

use avr_core::{DDRB, DDRC, DDRD, PINB, PINC, PIND, PORTB, PORTC, PORTD};

const BLACK: u8 = 0u8;
const RED: u8 = 1u8;
const GREEN: u8 = 2u8;
const YELLOW: u8 = 3u8;

const CPU_FREQUENCY_HZ: u64 = 16_000_000;
const BAUD: u64 = 9600;
const UBRR: u16 = (CPU_FREQUENCY_HZ / 16 / BAUD - 1) as u16;

const HIGH: u8 = 0xFF;
const LOW: u8 = 0x00;

const OUTPUT: u8 = 0xFF;
const INPUT: u8 = 0x00;

#[derive(Clone, PartialEq)]
struct Position {
    x: i8,
    y: i8,
}

#[derive(Clone, PartialEq)]
struct Size {
    width: i8,
    height: i8,
}

#[derive(PartialEq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Clone, PartialEq)]
struct Player {
    position: Position,
    old_position: Position,
    size: Size,
    force: i8,
    speed: f32,
    powered: bool,
    jumping: bool,
}
impl Player {
    fn new() -> Self {
        Player {
            position: Position { x: 2, y: 30 },
            old_position: Position { x: 2, y: 30 },
            size: Size {
                // width: 5,
                width: 3,
                height: 6,
            },
            force: 0,
            speed: 0.0,
            powered: false,
            jumping: true,
        }
    }

    fn collide(&self, wall: &Wall) -> Option<Direction> {
        if wall.size.width == 0 || wall.size.height == 0 {
            return None;
        }
        let is_colliding = self.position.x < wall.position.x + wall.size.width
            && self.position.x + self.size.width > wall.position.x
            && self.position.y < wall.position.y + wall.size.height
            && self.position.y + self.size.height > wall.position.y;

        if is_colliding {
            if self.position.y < wall.position.y {
                Some(Direction::Up)
            } else if self.position.x < wall.position.x {
                Some(Direction::Left)
            } else if self.position.x + self.size.width > wall.position.x + wall.size.width {
                Some(Direction::Right)
            } else {
                Some(Direction::Down)
            }
        } else {
            None
        }
        // =========== New Function ===========
        // if self.position.x + self.size.width > wall.position.x
        //     && self.position.x < wall.position.x + wall.size.width
        //     && self.position.y + self.size.height > wall.position.y
        //     && self.position.y < wall.position.y + wall.size.height
        // {
        //     if self.jumping && self.force <= 0 {
        //         Some(Direction::Down)
        //     } else {
        //         Some(Direction::Up)
        //     }
        // } else if self.position.x + self.size.width > wall.position.x
        //     && self.position.x < wall.position.x + wall.size.width
        //     && self.position.y + self.size.height > wall.position.y
        //     && self.position.y < wall.position.y + wall.size.height
        // {
        //     if self.position.x < wall.position.x {
        //         Some(Direction::Left)
        //     } else {
        //         Some(Direction::Up)
        //     }
        // } else {
        //     None
        // }
    }

    fn draw(&mut self) {
        // draw_player(&player, &old_player);
        // old_player = player.clone();
        if self.position == self.old_position {
            return;
        }
        for b in [
            '|' as u8,
            2,
            // self.old_position.x as u8 + 1,
            // self.old_position.y as u8,
            // self.size.width as u8 - 2,
            // self.size.height as u8 - 1,
            self.old_position.x as u8,
            self.old_position.y as u8,
            self.size.width as u8,
            self.size.height as u8 - 1,
            GREEN,
        ]
        .iter()
        {
            serial::transmit(*b);
        }
        for b in [
            '|' as u8,
            2,
            // self.position.x as u8 + 1,
            // self.position.y as u8,
            // self.size.width as u8 - 2,
            // self.size.height as u8 - 1,
            self.position.x as u8,
            self.position.y as u8,
            self.size.width as u8,
            self.size.height as u8 - 1,
            RED,
        ]
        .iter()
        {
            serial::transmit(*b);
        }
        self.old_position = self.position.clone();
    }

    fn powerup(&mut self, growup: bool) {
        if growup {
            self.position.y = self.position.y - 4;
            self.size.height = self.size.height + 4;
            self.powered = true;
        } else {
            self.position.y = self.position.y + 4;
            self.size.height = self.size.height - 4;
            self.powered = false;
        }
    }
}

#[derive(Clone, PartialEq)]
enum WallType {
    Solid,
    Breakable,
    Secret,
    PowerupBox,
    VerticalPipe,
    Powerup,
}

#[derive(Clone)]
struct Wall {
    position: Position,
    size: Size,
    wall_type: WallType,
    visible: bool,
}

impl Wall {
    fn new(size: Size, position: Position, wall_type: WallType) -> Self {
        if wall_type == WallType::Secret {
            Wall {
                position: position,
                size: size,
                wall_type: wall_type,
                visible: false,
            }
        } else {
            Wall {
                position: position,
                size: size,
                wall_type: wall_type,
                visible: true,
            }
        }
    }

    fn powerup(position: Position) -> Self {
        Wall {
            position: Position {
                x: position.x + 1,
                y: position.y,
            },
            size: Size {
                width: 3,
                height: 5,
            },
            wall_type: WallType::Powerup,
            visible: true,
        }
    }

    fn useless() -> Self {
        Wall {
            position: Position { x: 0, y: 0 },
            size: Size {
                width: 0,
                height: 0,
            },
            wall_type: WallType::Solid,
            visible: false,
        }
    }

    fn draw(&self) {
        if self.size.width == 0 || self.size.height == 0 {
            return;
        }
        for b in [
            '|' as u8,
            2,
            self.position.x as u8,
            self.position.y as u8,
            self.size.width as u8,
            self.size.height as u8,
            if self.wall_type == WallType::Powerup {
                RED
            } else {
                BLACK
            },
        ]
        .iter()
        {
            serial::transmit(*b);
        }

        match self.wall_type {
            WallType::Breakable | WallType::PowerupBox => {
                for b in [
                    '|' as u8,
                    2,
                    self.position.x as u8 + 1,
                    self.position.y as u8 + 2,
                    self.size.width as u8 - 2,
                    1,
                    GREEN,
                ]
                .iter()
                {
                    serial::transmit(*b);
                }
            }
            WallType::VerticalPipe => {
                for b in [
                    '|' as u8,
                    2,
                    self.position.x as u8 - 1,
                    self.position.y as u8,
                    self.size.width as u8 + 2,
                    4,
                    BLACK,
                ]
                .iter()
                {
                    serial::transmit(*b);
                }
            }
            WallType::Powerup => {
                for b in [
                    // First decoration
                    '|' as u8,
                    2,
                    self.position.x as u8 - 1,
                    self.position.y as u8 + 1,
                    5,
                    1,
                    RED,
                    // Second decoration
                    '|' as u8,
                    2,
                    self.position.x as u8 - 2,
                    self.position.y as u8 + 2,
                    7,
                    1,
                    RED,
                ]
                .iter()
                {
                    serial::transmit(*b);
                }
            }
            _ => {}
        }
    }

    fn erase(&self) {
        match self.wall_type {
            WallType::Powerup => {
                for b in [
                    '|' as u8,
                    2,
                    self.position.x as u8 - 2,
                    self.position.y as u8,
                    7,
                    5,
                    GREEN,
                ]
                .iter()
                {
                    serial::transmit(*b);
                }
            }
            _ => {
                for b in [
                    '|' as u8,
                    2,
                    self.position.x as u8,
                    self.position.y as u8,
                    self.size.width as u8,
                    self.size.height as u8,
                    GREEN,
                ]
                .iter()
                {
                    serial::transmit(*b);
                }
            }
        }
    }

    fn destroy(&mut self) {
        self.visible = false;
        self.erase();
    }
}

#[no_mangle]
pub unsafe extern "C" fn main() -> ! {
    serial_start();
    DDRB::set_mask_raw(HIGH);
    PORTB::unset_mask_raw(HIGH);
    DDRC::set_mask_raw(HIGH);
    PORTC::unset_mask_raw(HIGH);
    DDRD::set_mask_raw(HIGH);
    PORTD::unset_mask_raw(HIGH);

    let mut step: u8 = 0;
    let mut player = Player::new();
    let screen1: [Wall; 8] = [
        // Floor
        Wall::new(
            Size {
                width: 120,
                height: 4,
            },
            Position { x: 0, y: 55 },
            WallType::Solid,
        ),
        // Individual block left
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 50, y: 35 },
            WallType::Solid,
        ),
        // Powerup block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 70, y: 35 },
            WallType::PowerupBox,
        ),
        // Middle breakable block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 80, y: 35 },
            WallType::Breakable,
        ),
        // Right breakable block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 90, y: 35 },
            WallType::Breakable,
        ),
        // left middle solid block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 80, y: 20 },
            WallType::Solid,
        ),
        // right middle solid block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 85, y: 35 },
            WallType::Solid,
        ),
        // Upper solid block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 75, y: 35 },
            WallType::Solid,
        ),
    ];
    let screen2: [Wall; 8] = [
        // Floor
        Wall::new(
            Size {
                width: 120,
                height: 4,
            },
            Position { x: 0, y: 55 },
            WallType::Solid,
        ),
        // Pipe 1
        Wall::new(
            Size {
                width: 5,
                height: 10,
            },
            Position { x: 35, y: 45 },
            WallType::VerticalPipe,
        ),
        Wall::new(
            Size {
                width: 0,
                height: 5,
            },
            Position { x: 34, y: 45 },
            WallType::Solid,
        ),
        // Pipe 2
        Wall::new(
            Size {
                width: 5,
                height: 15,
            },
            Position { x: 65, y: 40 },
            WallType::VerticalPipe,
        ),
        Wall::new(
            Size {
                width: 0,
                height: 5,
            },
            Position { x: 64, y: 40 },
            WallType::Solid,
        ),
        // Pipe 3
        Wall::new(
            Size {
                width: 5,
                height: 20,
            },
            Position { x: 95, y: 35 },
            WallType::VerticalPipe,
        ),
        Wall::new(
            Size {
                width: 0,
                height: 5,
            },
            Position { x: 94, y: 35 },
            WallType::Solid,
        ),
        // Extra wall useless
        Wall::useless(),
    ];
    let screen3: [Wall; 8] = [
        // Floor 1
        Wall::new(
            Size {
                width: 80,
                height: 4,
            },
            Position { x: 0, y: 55 },
            WallType::Solid,
        ),
        // floor 2
        Wall::new(
            Size {
                width: 25,
                height: 4,
            },
            Position { x: 95, y: 55 },
            WallType::Solid,
        ),
        // Pipe
        Wall::new(
            Size {
                width: 5,
                height: 20,
            },
            Position { x: 30, y: 35 },
            WallType::VerticalPipe,
        ),
        // Invisible block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 65, y: 30 },
            WallType::Secret,
        ),
        // Extra walls useless
        Wall::useless(),
        Wall::useless(),
        Wall::useless(),
        Wall::useless(),
    ];
    let screen4: [Wall; 8] = [
        // Floor 1
        Wall::new(
            Size {
                width: 100,
                height: 4,
            },
            Position { x: 0, y: 55 },
            WallType::Solid,
        ),
        // Second floor
        Wall::new(
            Size {
                width: 5,
                height: 4,
            },
            Position { x: 115, y: 55 },
            WallType::Solid,
        ),
        // Powerup block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 50, y: 35 },
            WallType::Breakable,
        ),
        // Middle breakable block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 55, y: 35 },
            WallType::PowerupBox,
        ),
        // Right breakable block
        Wall::new(
            Size {
                width: 5,
                height: 5,
            },
            Position { x: 60, y: 35 },
            WallType::Breakable,
        ),
        // Up big block
        Wall::new(
            Size {
                width: 45,
                height: 5,
            },
            Position { x: 65, y: 15 },
            WallType::Solid,
        ),
        // Extra walls useless
        Wall::useless(),
        Wall::useless(),
    ];
    let map: [[Wall; 8]; 4] = [screen1, screen2, screen3, screen4];

    clear();
    let mut level: usize = 0;
    let mut walls = map[level].clone();
    // As the original SMB, in logic there will only be one powerup at a time
    let mut powerup: Option<Wall> = None;
    let mut powerup_painted = true;
    for wall in walls.iter() {
        if wall.visible {
            wall.draw();
        }
    }

    loop {
        // clear();

        // If the player fall of the map, restart
        if player.position.y > 70 {
            player = Player::new();
            continue;
        }
        // level scroll logic
        if player.position.x > 120 {
            level += 1;
            if level >= map.len() {
                level -= 1;
                player.position.x = 120;
                continue;
            }
            walls = map[level].clone();
            player.position.x = 1;
            clear();
            for wall in walls.iter() {
                if wall.visible {
                    wall.draw();
                }
            }
            continue;
        }
        if player.position.x < 0 && level > 0 {
            level -= 1;
            player.position.x = 120;
            walls = map[level].clone();
            clear();
            for wall in walls.iter() {
                if wall.visible {
                    wall.draw();
                }
            }
            continue;
        }

        match powerup {
            Some(ref powerup_) => {
                if player.collide(&powerup_).is_some() {
                    powerup_.erase();
                    powerup = None;
                    player.powerup(true);
                } else {
                    if !powerup_painted {
                        powerup_.draw();
                        powerup_painted = true;
                    }
                }
            }
            _ => {}
        }

        let mut fall = true;

        // Static wall behavior
        for wall in &mut walls {
            if wall.visible || wall.wall_type == WallType::Secret {
                match player.collide(wall) {
                    Some(Direction::Up) => {
                        player.jumping = false;
                        player.force = 0;
                        while !player.collide(&wall).is_none() {
                            player.position.y -= 1;
                        }
                        player.position.y += 1;
                        fall = false;
                        wall.draw();
                    }
                    Some(Direction::Down) => {
                        player.force = 0;
                        player.position.y += 1;
                        fall = true;
                        match wall.wall_type {
                            WallType::Breakable => {
                                if player.powered {
                                    wall.destroy();
                                } else {
                                    wall.draw();
                                }
                            }
                            WallType::PowerupBox => {
                                let powerup_ = Wall::powerup(Position {
                                    x: wall.position.x,
                                    y: wall.position.y - 5,
                                });
                                powerup = Some(powerup_);
                                powerup_painted = false;
                                wall.wall_type = WallType::Solid;
                            }
                            WallType::Secret => {
                                if player.force < 0 {
                                    wall.visible = true;
                                    wall.wall_type = WallType::Solid;
                                    wall.draw();
                                }
                            }
                            _ => wall.draw(),
                        }
                    }
                    Some(dir) => {
                        while !player.collide(&wall).is_none() {
                            if dir == Direction::Left {
                                player.position.x -= 1;
                            } else {
                                player.position.x += 1;
                            }
                        }
                        if dir == Direction::Left {
                            player.position.x += 1;
                        } else {
                            player.position.x -= 1;
                        }
                        wall.draw();
                        // draw_wall(&wall);
                    }
                    // None => fall = true,
                    None => {}
                }
            }
        }

        player.draw();

        // Gravity control
        // if the variable `fall` is true it's because all the logic of
        // The platforms doesn't stop the fallen of the character
        if fall {
            step = (step + 1) % 4;
            if step == 0 {
                player.force += 1;
            }
        } else {
            step = 0;
        }
        player.position.y += player.force;
        // player.position.x += 2;

        let values = PINB::read();
        let left = (values >> 0) % 2 == 1;
        let right = (values >> 1) % 2 == 1;
        let jump = (values >> 2) % 2 == 1;

        if left {
            player.position.x += 1;
        }
        if right {
            player.position.x -= 1;
        }
        if jump && !player.jumping {
            player.position.y -= 1;
            player.force = -3;
            player.jumping = true;
        }

        let values = PINB::read();
        for n in 0..8 {
            if (values >> n) % 2 == 1 {
                serial::transmit('1' as u8);
            } else {
                serial::transmit('0' as u8);
            }
        }
        // serial::transmit('\n' as u8);
        // let values = PINC::read();
        // for n in 0..8 {
        //     if (values >> n) % 2 == 1 {
        //         serial::transmit('1' as u8);
        //     } else {
        //         serial::transmit('0' as u8);
        //     }
        // }
        // serial::transmit('\n' as u8);
        // let values = PIND::read();
        // for n in 0..8 {
        //     if (values >> n) % 2 == 1 {
        //         serial::transmit('1' as u8);
        //     } else {
        //         serial::transmit('0' as u8);
        //     }
        // }
        // serial::transmit('\n' as u8);

        // delay(20);
    }
}

fn _delay(cycles: u32) {
    for _ in 0..(1_600 * cycles) {
        unsafe { llvm_asm!("" :::: "volatile") }
    }
}

fn clear() {
    for b in ['|' as u8, 0, 2].iter() {
        serial::transmit(*b);
    }
}

fn serial_start() {
    serial::Serial::new(UBRR)
        .character_size(serial::CharacterSize::EightBits)
        .mode(serial::Mode::Asynchronous)
        // .mode(serial::Mode::MasterSpi)
        .parity(serial::Parity::Disabled)
        .stop_bits(serial::StopBits::OneBit)
        .configure();
}

// These do not need to be in a module, but we group them here for clarity.
// pub mod std {
// #[lang = "eh_personality"]
// pub unsafe extern "C" fn rust_eh_personality(
// _state: (),
// _exception_object: *mut (),
// _context: *mut (),
// ) -> () {
// }
//
// #[panic_handler]
// fn panic(_info: &::core::panic::PanicInfo) -> ! {
// loop {}
// }
// }
